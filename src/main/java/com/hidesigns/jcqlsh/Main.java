/*
 * License: MIT
 * Copyright (c) 2016 HiDESIGNS co ltd
 */
package com.hidesigns.jcqlsh;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.commons.cli.*;

/**
 * Cassandra cqlsh replacement written in Java instead of Python.
 * @author Tanner Mickelson
 */
public class Main 
{
    public static void main(String[] args) throws Exception 
    {
        Options options = new Options();
        
        //Node address is used as the contact point, defaults to 127.0.0.1
        Option nodeOption = new Option("n", "node", true, "IP address of node to try connecting to");
        nodeOption.setRequired(false);
        options.addOption(nodeOption);

        //Keyspace to connect to, default is null
        Option keyspaceOption = new Option("k", "keyspace", true, "Keyspace to initialize the driver with (leave empty to connect without a keyspace).");
        keyspaceOption.setRequired(false);
        options.addOption(keyspaceOption);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        //Try parsing the commands
        try 
        {
            cmd = parser.parse(options, args);
        } 
        catch (ParseException e) 
        {
            System.out.println(e.getMessage());
            formatter.printHelp("jcqlsh", options);

            System.exit(1);
            return;
        }

        String contactPoint = cmd.getOptionValue("node", "127.0.0.1");
        String keyspace = cmd.getOptionValue("keyspace", null);

        System.out.println("Connecting to: "+contactPoint);
        Cluster cluster = Cluster.builder().addContactPoint(contactPoint).build();
        Session session = null;
        
        if (keyspace != null)
        {
            //Connect to keyspace if specified
            System.out.println("Using keyspace: "+keyspace);
            session = cluster.connect(keyspace);
        }
        else
        {
            //Otherwise if no keyspace was passed, just connect to the whole cluster
            System.out.println("No keyspace specified.");
            session = cluster.connect();
        }
        
        //Start main loop for shell
        boolean exitFlag = false;
        while (!exitFlag)
        {
            //Read command from console
            System.out.print("jcqlsh> ");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String input = br.readLine();
            if (input == null || input.length() == 0)
            {
                continue;
            }
            
            //Check for special commands
            if (input.compareToIgnoreCase("exit") == 0)
            {
                exitFlag = true;
                continue;
            }
            else if (input.compareToIgnoreCase("describe keyspaces") == 0)
            {
                input = "SELECT * FROM system_schema.keyspaces;";
            }
            else if (input.compareToIgnoreCase("describe keyspaces") == 0)
            {
                System.out.print("Keyspace name: ");
                String keyspaceName = br.readLine();
                input = "create keyspace "+keyspaceName+" with replication={'class':'SimpleStrategy', 'replication_factor':1};";
            }
            
            //Run CQL
            try
            {
                ResultSet result = session.execute(input);
                if (result != null)
                {
                    System.out.println(result.getColumnDefinitions());
                    for (Row row : result.all())
                    {
                        System.out.println(row);
                    }
                }
                else
                {
                    System.out.println("no result.");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        
        cluster.close();
    }
}
